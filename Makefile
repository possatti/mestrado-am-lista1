#!/usr/bin/make -f
SHELL=/bin/bash
.ONESHELL:
.PHONY: all clean q1a q1b q1c q1d q2a q2b q2c q3a q3b q5a q5b q6a q6b q8a q8b q8c q9 q10a q10b q10c


REPORT_PDF = ./relatorio/relatorio.pdf
REPORT_SOURCE = ./relatorio/relatorio.tex
SOURCE_LIST = $(shell find src/ -regex ".*\.\(py\|png\)")
ZIP_PACKAGE = lista1.zip


all: $(ZIP_PACKAGE)

$(ZIP_PACKAGE): $(REPORT_PDF) $(SOURCE_LIST) README.md Makefile
	zip -r $(@) $(?)

$(REPORT_PDF): $(REPORT_SOURCE)
	$(MAKE) --directory relatorio

clean:
	rm -f $(ZIP_PACKAGE)


## How to run each question ##
q1a:
	python src/questao-1/letra-a.py
q1b:
	python src/questao-1/letra-b.py
q1c:
	python src/questao-1/letra-c.py
q1d:
	python src/questao-1/letra-d.py

q2a:
	python src/questao-2/main.py
q2b:
	python src/questao-2/main.py -w
q2c:
	python src/questao-2/main.py -w -1

q3a:
	python src/questao-3/main.py
q3b:
	python src/questao-3/main.py --pre-process

q5a:
	python src/questao-5/main.py
q5b:
	python src/questao-5/main.py

q6a:
	python src/questao-6/main.py -a
q6b:
	python src/questao-6/main.py -b

q8a:
	python src/questao-8/main.py --create-figures
q8b:
	python src/questao-8/main.py --create-figures
q8c:
	python src/questao-8/main.py

q9:
	python src/questao-9/main.py

q10a:
	python src/questao-x/main.py -a
q10b:
	python src/questao-x/main.py -b
q10c:
	python src/questao-x/main.py -c
	python src/questao-x/main.py -r
