Lista 1 de Aprendizagem de Máquinas
===================================

Essa é a minha resolução das questões da primeira lista da disciplina de Aprendizagem de Máquinas. O relatório se encontra em `relatorio/relatorio.pdf`. E os códigos fontes se encontram em `src/`.


## Instruções

Todos os algoritmos foram desenvolvidos para o Python 3. Apesar de eu ter tido os cuidados para que o código funcionasse também no Python 2, mas para o Python 2 eu não testei.
Também é importante instalar o `numpy` e o `matplotlib`. Eu utilizo essas bibliotecas em praticamente todos as resoluções.

No Ubuntu, você pode facilmente instalar os pré-requisitos ao instalar a distribuição [Anaconda](https://www.continuum.io/downloads). Alternativamente, se você já tiver o Python instalado, deveria ser capaz de instalar as bibliotecas usando o comando `pip install numpy matplotlib`.

Antes de executar os códigos fontes, é importante instalar também o pacote `am` que eu mesmo desenvolvi. Desenvolvi esse pacote para encapsular os algoritmos em comum que a maioria das questões utilizam, como o Nearest Neighbor e o PCA. Para instalar o `am`, da raiz do trabalho execute o comando `pip install src/am`.


## Questões

Eu organizei a estrutura dos scripts de forma que os itens relacionados a questão *X* estivessem dentro do diretório `src/questao-X`. E criei no `Makefile` uma espécie de índice de como executar cada questão. Se você quiser executar a questão *1.c*, execute `make q1c`. Se quiser executar a questão *10.c*, execute `make q10c`.


## TODO

1.  a[x] b[x] c[x] d[x]
2.  a[x] b[x] c[x]
3.  a[x] b[x] c[x]
4.   [x]
5.  a[x] b[x] c[ ] d[ ]
6.  a[x] b[x] c[ ]
7.   [x]
8.  a[x] b[x] c[x]
9.   [x]
10. a[x] b[x] c[x]
