#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.regression import linear_regression
from am.utils import root_mean_square_error, mean_absolute_percentage_error, coefficient_of_determination

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)

def main():
	np.random.seed(7)
	dataset = np.genfromtxt(args.polinomio_dataset)
	np.random.shuffle(dataset)
	dataset_n = len(dataset)
	train_n = int(dataset_n*0.7)

	train = dataset[:train_n,:]
	train_x = train[:,0]
	train_y = train[:,1]
	test = dataset[train_n:,:]
	test_x = test[:,0]
	test_y = test[:,1]

	# Linear model
	if args.letra_a:
		A = train_x[:,np.newaxis] ** np.arange(2)
		w = linear_regression(A, train_y)
		b, a = w

		train_fx = (train_x[:,np.newaxis]**np.arange(2)).dot(w)
		test_fx = (test_x[:,np.newaxis]**np.arange(2)).dot(w)
		print('Linear model: y = {:.4f} * x + {:.4f}'.format(a, b))
		print('Model coefficients:', *['w{:d}={:.4f}'.format(i, wi) for i, wi in enumerate(w)])
		print('RMSE using training data:', root_mean_square_error(train_x, train_y, train_fx))
		print('RMSE using test data:', root_mean_square_error(test_x, test_y, test_fx))
		print('MAPE using training data:', mean_absolute_percentage_error(train_x, train_y, train_fx))
		print('MAPE using test data:', mean_absolute_percentage_error(test_x, test_y, test_fx))
		print('R2:', coefficient_of_determination(test_x, test_y, test_fx))

		# Plot the points, and the linear model.
		fig, ax = plt.subplots()
		ax.scatter(train_x, train_y, c='blue', marker='o', label='Train')
		ax.scatter(test_x, test_y, c='green', marker='o', label='Test')
		ax.plot(train_x, A.dot(w), '-', c='black', label='Linear Model')
		ax.legend()
		if args.save:
			plt.savefig(args.save)
		else:
			plt.show()

	# Polinomial model.
	if args.letra_b:
		As = []
		ws = []
		r2s = []
		for degree in range(2,10):
			A = train_x[:,np.newaxis] ** np.arange(degree)
			w = linear_regression(A, train_y)
			test_fx = test_x[:,np.newaxis]**np.arange(degree).dot(w)
			r2 = coefficient_of_determination(test_x, test_y, test_fx)
			As.append(A)
			ws.append(w)
			r2s.append(r2)

		r2s = np.array(r2s)
		min_index = r2s.argmin()

		min_train_fx = train_x[:,np.newaxis]**np.arange(len(ws[min_index])).dot(ws[min_index])
		min_test_fx = test_x[:,np.newaxis]**np.arange(len(ws[min_index])).dot(ws[min_index])
		print('Best model coefficients:', *['w{:d}={:.4f}'.format(i, wi) for i, wi in enumerate(ws[min_index])])
		print('RMSE using training data:', root_mean_square_error(train_x, train_y, min_train_fx))
		print('RMSE using test data:', root_mean_square_error(test_x, test_y, min_test_fx))
		print('MAPE using training data:', mean_absolute_percentage_error(train_x, train_y, min_train_fx))
		print('MAPE using test data:', mean_absolute_percentage_error(test_x, test_y, min_test_fx))


		# Plot the points, and the linear model.
		fig, ax = plt.subplots()
		ax.scatter(train_x, train_y, c='blue', marker='o', label='Train')
		ax.scatter(test_x, test_y, c='green', marker='o', label='Test')
		sorted_indexes = train_x.argsort()
		xs = train_x[sorted_indexes]
		ys = As[min_index].dot(ws[min_index])[sorted_indexes]
		ax.plot(xs, ys, '-', c='black', label='Polinomial Model')
		ax.legend()
		if args.save:
			plt.savefig(args.save)
		else:
			plt.show()

	# Polinomial model.
	if args.letra_c:
		As = []
		ws = []
		r2s = []
		for degree in range(2,10):
			A = train_x[:,np.newaxis] ** np.arange(degree)
			w = linear_regression(A, train_y)
			test_fx = test_x[:,np.newaxis]**np.arange(degree).dot(w)
			r2 = coefficient_of_determination(test_x, test_y, test_fx)
			As.append(A)
			ws.append(w)
			r2s.append(r2)

		r2s = np.array(r2s)
		min_index = r2s.argmin()

		min_train_fx = train_x[:,np.newaxis]**np.arange(len(ws[min_index])).dot(ws[min_index])
		min_test_fx = test_x[:,np.newaxis]**np.arange(len(ws[min_index])).dot(ws[min_index])
		print('Best model coefficients:', *['w{:d}={:.4f}'.format(i, wi) for i, wi in enumerate(ws[min_index])])
		print('RMSE using training data:', root_mean_square_error(train_x, train_y, min_train_fx))
		print('RMSE using test data:', root_mean_square_error(test_x, test_y, min_test_fx))
		print('MAPE using training data:', mean_absolute_percentage_error(train_x, train_y, min_train_fx))
		print('MAPE using test data:', mean_absolute_percentage_error(test_x, test_y, min_test_fx))


		# Plot the points, and the linear model.
		fig, ax = plt.subplots()
		ax.scatter(train_x, train_y, c='blue', marker='o', label='Train')
		ax.scatter(test_x, test_y, c='green', marker='o', label='Test')
		sorted_indexes = train_x.argsort()
		xs = train_x[sorted_indexes]
		ys = As[min_index].dot(ws[min_index])[sorted_indexes]
		ax.plot(xs, ys, '-', c='black', label='Polinomial Model')
		ax.legend()
		if args.save:
			plt.savefig(args.save)
		else:
			plt.show()



if __name__ == '__main__':
	# Default paths.
	root_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..'))
	default_polinomio_location = os.path.join(root_dir, 'bases', 'Polinomio.txt')

	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--polinomio-dataset', default=default_polinomio_location, help='Path to the dataset.')
	parser.add_argument('-s', '--save', metavar='file_path', help='Save the produced figure to the specified path.')

	letters_group = parser.add_mutually_exclusive_group(required=True)
	letters_group.add_argument('-a', '--letra-a', action='store_true', help='Run the solution for letter a.')
	letters_group.add_argument('-b', '--letra-b', action='store_true', help='Run the solution for letter b.')
	letters_group.add_argument('-c', '--letra-c', action='store_true', help='Run the solution for letter c.')

	args = parser.parse_args()

	main()
