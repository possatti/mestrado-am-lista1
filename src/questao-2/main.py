#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.dimensionality import pca, CORRELATION, COVARIANCE

import matplotlib.pyplot as plt
import numpy as np
import argparse
import fnmatch
import json
import sys
import re
import os

print(sys.version, file=sys.stderr)

# Colors and markers that can be used with matplotlib.
COLORS = list(reversed(['blue', 'red', 'yellow', 'black', 'green', 'cyan', 'magenta', 'white']))
MARKERS = list(reversed(['o', 's', '^', 'p', 'h', '8', 'D', 'x']))

def whitening(data):
	"""
	Apply a whitening transformation to the data.
	  - https://en.wikipedia.org/wiki/Whitening_transformation#Definition
	"""
	P = np.corrcoef(data, rowvar=False)
	V = np.diag(np.var(data, axis=0))
	W = P**(-1/2) / V**(-1/2)
	return data.dot(W)

def main():
	# Read data from file.
	with open(args.cnae_file, 'r') as f:
		lines = f.readlines()
		labels = list(map(lambda l: int(l[0]), lines))
		labels_unique = list(set(labels))
		data = list(map(lambda l: l.strip()[1:].split(), lines))
		data_np = np.array(data, dtype=int)

	print('Calculating PCA...', file=sys.stderr)
	pcs = pca(data_np, usenumpy=args.use_numpy, method=COVARIANCE)

	if args.whitening:
		print('Applying whitening...', file=sys.stderr)
		pcs = whitening(pcs)
		print('DEBUG: Correlation matrix after whitening:\n',
			np.corrcoef(pcs, rowvar=False).real, file=sys.stderr) # debug

	print('Preparing figure...', file=sys.stderr)
	axes = []
	if args.single_pc:
		for label in labels_unique:
			this_label_data = list(filter(lambda instance: instance[0] == label, zip(labels, pcs)))
			xs = list(map(lambda instance: instance[1][0], this_label_data))
			ys = [ 0 for _ in xs ]
			if label == 5:
				size = 100
			else:
				size = 400
			ax = plt.scatter(xs, ys, s=size, c=COLORS.pop(), marker='|')
			axes.append(ax)
	else:
		for label in labels_unique:
			this_label_data = list(filter(lambda instance: instance[0] == label, zip(labels, pcs)))
			xs = list(map(lambda instance: instance[1][0], this_label_data))
			ys = list(map(lambda instance: instance[1][1], this_label_data))
			ax = plt.scatter(xs, ys, s=30, c=COLORS.pop(), marker=MARKERS.pop())
			axes.append(ax)

	plt.legend(axes, labels_unique)
	# plt.title('')
	plt.xlabel('PC 1')
	if not args.single_pc:
		plt.ylabel('PC 2')

	if args.save:
		print('Saving figure to {}.'.format(args.save), file=sys.stderr)
		plt.savefig(args.save)
	else:
		print('Showing figure.', file=sys.stderr)
		plt.show()

# Arguments and options
if __name__ == '__main__':
	SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
	default_cnae_location = os.path.join(SCRIPT_DIR, '..', '..', 'bases', 'CNAE-9_reduzido.txt')
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--cnae-file', default=default_cnae_location, help='Base do CNAE-9_reduzido.')
	parser.add_argument('-w', '--whitening', action='store_true', help='Apply whitening after PCA.')
	parser.add_argument('-n', '--use-numpy', action='store_true', help='Use numpy instead of my own functions.')
	parser.add_argument('-s', '--save', metavar='file_path', help='Save the produced figure to the specified file path.')
	parser.add_argument('-1', '--1-PC', action='store_true', dest='single_pc', help='Produce a figure with only one principal component.')
	args = parser.parse_args()
	main()
