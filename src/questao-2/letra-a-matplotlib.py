#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from matplotlib.mlab import PCA

import matplotlib.pyplot as plt
import numpy as np
import argparse
import fnmatch
import json
import sys
import re
import os

print(sys.version, file=sys.stderr)

# Colors and markers that can be used with matplotlib.
COLORS = list(reversed(['blue', 'red', 'yellow', 'black', 'green', 'cyan', 'magenta', 'white']))
MARKERS = list(reversed(['o', 's', '^', 'p', 'h', '8', 'D', 'x']))

def main():
	# Read data from file.
	with open(args.cnae_file, 'r') as f:
		lines = f.readlines()
		labels = list(map(lambda l: int(l[0]), lines))
		labels_unique = list(set(labels))
		data = list(map(lambda l: l.strip()[1:].split(), lines))
		data_np = np.array(data, dtype=int)

	result = PCA(data_np)

	axes = []
	for label in labels_unique:
		this_label_data = list(filter(lambda instance: instance[0] == label, zip(labels, result.Y)))
		xs = list(map(lambda instance: instance[1][0], this_label_data))
		ys = list(map(lambda instance: instance[1][1], this_label_data))
		ax = plt.scatter(xs, ys, s=30, c=COLORS.pop(), marker=MARKERS.pop())
		axes.append(ax)

	plt.legend(axes, labels_unique)
	# plt.title('Iris Database')
	plt.xlabel('PC 1')
	plt.ylabel('PC 2')

	if args.save:
		plt.savefig(args.save)
	else:
		plt.show()

# Arguments and options
if __name__ == '__main__':
	SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
	default_cnae_location = os.path.join(SCRIPT_DIR, '..', '..', 'bases', 'CNAE-9_reduzido.txt')
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--cnae-file', default=default_cnae_location, help='Base do CNAE-9_reduzido.')
	parser.add_argument('-s', '--save', metavar='file_path', help='Save the graph to the specified file path.')
	args = parser.parse_args(['-s', 'letra-a-matplotlib'])
	main()