#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.classification import NN, Rocchio
from am.utils import confusion_matrix
from am.dimensionality import pca

import numpy as np
import argparse
import math
import sys
import re
import os


print(sys.version, file=sys.stderr)

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

def read_nebulosa_discarding_missing_values(file_path):
	with open(file_path, 'r') as f:
		lines = f.readlines()
	# Remove lines with missing data. They are just a few.
	lines = list(filter(lambda l: not re.search(r'\?', l), lines))
	# Cast everything to float.
	instances = list(map(lambda l: l.strip().split(), lines))
	instances_np = np.array(instances, dtype=float)
	data = instances_np[:,:-1]
	labels = instances_np[:,-1].astype(dtype=int)
	return data, labels

def read_and_preprocess_nebulosa(file_path):
	cols_names = "id,nome,porte,educacao,socioeconomico,nascimento,idade,label"

	data = np.genfromtxt(file_path)


	# Some values are very close to their proper range, but fall outside of it.
	# Let's try to round them up.
	data[data[:,2] <1, 2] = np.around(data[data[:,2] <1, 2])
	data[data[:,2] >3, 2] = np.around(data[data[:,2] >3, 2])
	data[data[:,3] <1, 3] = np.around(data[data[:,3] <1, 3])
	data[data[:,3] >4, 3] = np.around(data[data[:,3] >4, 3])
	data[data[:,4] <1, 4] = np.around(data[data[:,4] <1, 4])
	data[data[:,4] >4, 4] = np.around(data[data[:,4] >4, 4])
	data[data[:,5] <1, 5] = np.around(data[data[:,5] <1, 5])
	data[data[:,5] >4, 5] = np.around(data[data[:,5] >4, 5])
	data[data[:,6] <1, 6] = np.around(data[data[:,6] <1, 6])
	data[data[:,6] >4, 6] = np.around(data[data[:,6] >4, 6])

	# Set values out of range to nan. Later they will be set to the mean of that class.

	# DEBUG: Some of the values out of range:
	# print(data[:,2:7][data[:,2:7] < 1])
	# print(data[:,2:7][data[:,2:7] > 4])

	data[data[:,2] <1, 2] = np.nan
	data[data[:,2] >3, 2] = np.nan
	data[data[:,3] <1, 3] = np.nan
	data[data[:,3] >4, 3] = np.nan
	data[data[:,4] <1, 4] = np.nan
	data[data[:,4] >4, 4] = np.nan
	data[data[:,5] <1, 5] = np.nan
	data[data[:,5] >4, 5] = np.nan
	data[data[:,6] <1, 6] = np.nan
	data[data[:,6] >4, 6] = np.nan

	# 'nascimento' and 'idade' are redundant, so we copy from one another when
	# any of them is missing.
	data[np.isnan(data[:,5]),5] = data[np.isnan(data[:,5]),6]
	data[np.isnan(data[:,5]),6] = data[np.isnan(data[:,5]),5]

	# Complete data using the means for the features of that class.
	labels = data[:,-1]
	labels_unique = np.unique(labels)
	labels_unique = labels_unique[~np.isnan(labels_unique)] # Remove nan
	for label in labels_unique:
		this_label_data = data[labels == label]
		means = np.nanmean(this_label_data, axis=0)
		print('Means for label {:.0f}:\n'.format(label), means)
		data[labels == label] = np.where(np.isnan(this_label_data), means, this_label_data)

	# Discard instances with nan values that could not be filled in any other way.
	print('Rows with NaN values that will be discarded:\n', data[np.isnan(data).any(axis=1)], file=sys.stderr)
	data = data[~np.isnan(data).any(axis=1)]

	# Discard instances with any negative value.
	print('Rows with negative values that will be discarded:\n', data[(data < 0).any(axis=1)], file=sys.stderr)
	data = data[~(data < 0).any(axis=1)]

	# Discards 'id', 'nome' and 'idade'.
	data = data[:,[2,3,4,5,7]]

	print('Rows with way too big values:\n', data[(data > 10).any(axis=1),:])

	return data[:,:-1], data[:,-1].astype(int)

def main():
	if args.pre_process:
		print('Loading {} and pre-processing...'.format(args.nebulosa_train), file=sys.stderr)
		train_data, train_labels = read_and_preprocess_nebulosa(args.nebulosa_train)
		# print('Loading {} and pre-processing...'.format(args.nebulosa_test), file=sys.stderr)
		# test_data, test_labels = read_and_preprocess_nebulosa(args.nebulosa_test)
		print('Loading {} without pre-processing...'.format(args.nebulosa_test), file=sys.stderr)
		test_data, test_labels = read_nebulosa_discarding_missing_values(args.nebulosa_test)
		test_data = test_data[:,[2,3,4,5]]
	else:
		print('Loading {}...'.format(args.nebulosa_train), file=sys.stderr)
		train_data, train_labels = read_nebulosa_discarding_missing_values(args.nebulosa_train)
		print('Loading {}...'.format(args.nebulosa_test), file=sys.stderr)
		test_data, test_labels = read_nebulosa_discarding_missing_values(args.nebulosa_test)

	print('\nTraining and testing using NN...', file=sys.stderr)
	nn = NN()
	nn.train(train_data, train_labels)
	nn_predictions = nn.predict(test_data)

	# Metrics for NN.
	print('NN predictions on test data:', nn_predictions)
	nn_conf = confusion_matrix(nn_predictions, test_labels, [1, 2, 3])
	print('NN confusion matrix:\n', nn_conf)
	print('NN accuracy: {}'.format(nn_conf.trace() / nn_conf.sum()))

	print('\nTraining and testing using Rocchio...', file=sys.stderr)
	rocchio = Rocchio()
	rocchio.train(train_data, train_labels)
	rocchio_predictions = rocchio.predict(test_data)

	# Metrics for Rocchio.
	print('Rocchio predictions on test data:', rocchio_predictions)
	roc_conf = confusion_matrix(rocchio_predictions, test_labels, [1, 2, 3])
	print('Rocchio confusion matrix:\n', roc_conf)
	print('Rocchio accuracy: {}'.format(roc_conf.trace() / roc_conf.sum()))

	if args.pca:
		import matplotlib.pyplot as plt

		# Prepare the centroids.
		centroids = np.array(list(rocchio.centroids.values()))
		centroids_labels = np.array(list(rocchio.centroids.keys()))

		# Prepare colors.
		label_to_color = {
			1: 'blue',
			2: 'green',
			3: 'red',
		}
		train_colors = np.vectorize(lambda l: label_to_color[l])(train_labels)
		# test_colors = np.vectorize(lambda l: label_to_color[l])(test_labels)
		centroids_colors = np.vectorize(lambda l: label_to_color[l])(centroids_labels)

		# Process the pca
		pcs = pca(np.vstack((train_data, centroids)))
		# pcs = pca(np.vstack((train_data, test_data, centroids)))

		# Plot the instances.
		# end_train_i = len(train_labels)
		# end_test_i = len(train_labels) + len(test_labels)
		# plt.scatter(pcs[0:end_train_i,0],                pcs[0:end_train_i,1], c=train_colors, marker='o', s=30)
		# plt.scatter(pcs[end_train_i:end_test_i,0], pcs[end_train_i:end_test_i,1], c=test_colors, marker='^', s=30)
		plt.scatter(pcs[0:-3,0], pcs[0:-3,1], c=train_colors, marker='o', s=30)

		# Plot the centroids.
		# plt.scatter(pcs[end_test_i:,0], pcs[end_test_i:,1], c=centroids_colors, marker='+', s=800)
		plt.scatter(pcs[-3:,0], pcs[-3:,1], c=centroids_colors, marker='+', s=800)

		plt.show()


# Arguments and options
if __name__ == '__main__':
	default_train_location = os.path.realpath(os.path.join(SCRIPT_DIR, '..', '..', 'bases', 'nebulosa_train.txt'))
	default_test_location = os.path.realpath(os.path.join(SCRIPT_DIR, '..', '..', 'bases', 'nebulosa_test.txt'))
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--nebulosa-train', default=default_train_location)
	parser.add_argument('--nebulosa-test', default=default_test_location)
	parser.add_argument('-p', '--pre-process', action='store_true', help='Do some pre-processing on the dataset.')
	parser.add_argument('--pca', action='store_true')
	args = parser.parse_args()
	main()
