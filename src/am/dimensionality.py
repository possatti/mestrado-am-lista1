from am.utils import covariance_matrix, correlation_matrix
import numpy as np

# Methods that can be used when calculating the pca.
COVARIANCE = 1
CORRELATION = 2
SCATTER = 3

METHODS = [
	COVARIANCE,
	CORRELATION,
	# SCATTER,
]


def pca(data, method=COVARIANCE, usenumpy=False):
	"""Returns a matrix with the data projected on the principal components."""

	assert method in METHODS, 'Invalid method value ({}).'.format(method)
	if usenumpy:
		if method == COVARIANCE:
			cov_matrix = np.cov(data, rowvar=False)
			eig_values, eig_vectors = np.linalg.eig(cov_matrix)
		elif method == CORRELATION:
			corr_matrix = np.corrcoef(data, rowvar=False)
			eig_values, eig_vectors = np.linalg.eig(corr_matrix)
	else:
		if method == COVARIANCE:
			cov_matrix = covariance_matrix(data, rowvar=False)
			eig_values, eig_vectors = np.linalg.eig(cov_matrix)
		elif method == CORRELATION:
			corr_matrix = correlation_matrix(data, rowvar=False)
			eig_values, eig_vectors = np.linalg.eig(corr_matrix)

	# Sort the eigen values and vectors.
	eig_pairs = [ (eig_values[i], eig_vectors[:,i]) for i in range(len(eig_values)) ]
	eig_pairs.sort(key=lambda pair: pair[0], reverse=True)
	sorted_eig_vetors = np.hstack(map(lambda p: p[1][np.newaxis].transpose(), eig_pairs))

	# Transform data.
	pcs = data.dot(sorted_eig_vetors)
	return pcs
