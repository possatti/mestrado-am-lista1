from am.utils import euclidean_distance, cosine_similarity, find_centroid
from functools import partial
import numpy as np

class NN(object):
	"""Nearest neighbor classification algorithm."""
	EUCLIDEAN_DISTANCE = 1
	COSINE_SIMILARITY = 2

	def train(self, instances, labels):
		self.training_instances = instances
		self.training_labels = labels

	def predict_one(self, instance, method=EUCLIDEAN_DISTANCE):
		"""Predict label for a single instance."""
		if method == NN.EUCLIDEAN_DISTANCE:
			distances = []
			for t_instance in self.training_instances:
				distances.append(euclidean_distance(instance, t_instance))
			closest_index = np.argmin(distances)
			preditected_label = self.training_labels[closest_index]
		elif method == NN.COSINE_SIMILARITY:
			similarities = []
			for t_instance in self.training_instances:
				similarities.append(cosine_similarity(instance, t_instance))
			closest_index = np.argmax(similarities)
			preditected_label = self.training_labels[closest_index]
		else:
			raise ValueError('You must pick a valid method.')
		return preditected_label

	def predict(self, instances, method=EUCLIDEAN_DISTANCE):
		"""Predict label for multiple instances."""
		predict = partial(self.predict_one, method=method) # Binds the specified method.
		return np.asarray(list(map(predict, instances)))

class KNN(object):
	"""K-Nearest-neighbor classification algorithm."""

	# def __init__(self, k):
	# 	self.k = k

	def train(self, training_instances, training_labels, validation_instances, validation_labels, ks=np.arange(1,10)):
		assert len(training_instances) == len(training_labels), 'Sizes should be equal ({}!={})'.format(len(training_instances), len(training_labels))
		assert len(validation_instances) == len(validation_labels), 'Sizes should be equal ({}!={})'.format(len(validation_instances), len(validation_labels))
		self.training_instances = training_instances
		self.training_labels = training_labels
		self.training_n = len(training_instances)
		self.validation_instances = validation_instances
		self.validation_labels = validation_labels
		self.validation_n = len(validation_instances)

		# Cache distances between training and valitation sets.
		distances = np.empty(shape=(self.training_n,self.validation_n))
		for i, training_instance in enumerate(training_instances):
			for j, validation_instance in enumerate(validation_instances):
				distances[i,j] = euclidean_distance(training_instance, validation_instance)
		# print('distances:\n', distances)

		# Choose best k.
		accs = np.empty(ks.shape)
		for i, k in enumerate(ks):
			sorting = np.argsort(distances, axis=0)
			k_indexes = sorting[:k,:]
			predicted_labels = np.empty(shape=(self.validation_n))
			for v in range(self.validation_n):
				k_labels = self.training_labels[k_indexes[:,v]]
				counts = np.bincount(k_labels)
				predicted_labels[v] = np.argmax(counts)
			acc = (validation_labels == predicted_labels).sum() / self.validation_n
			accs[i] = acc
		best_k = ks[np.argmax(accs)]

		self.k = best_k
		self.ks = ks
		self.accs = accs

	def predict_one(self, testing_instance, k=None):
		"""Predict label for a single instance."""
		k = k or self.k
		distances = []
		for t_instance in self.training_instances:
			distances.append(euclidean_distance(testing_instance, t_instance))
		sorted_indexes = np.argsort(distances)
		k_labels = self.training_labels[sorted_indexes[:self.k]]
		counts = np.bincount(k_labels)
		preditected_label = np.argmax(counts)
		return preditected_label

	def predict(self, testing_instances, k=None):
		"""Predict label for multiple instances."""
		k = k or self.k
		predict_with_k = partial(self.predict_one, k=k)
		return np.asarray(list(map(predict_with_k, testing_instances)))

class Rocchio(object):
	"""Rocchio classification algorithm."""

	def train(self, instances, labels):
		self.instances = instances
		self.labels = labels
		self.centroids = {}
		labels_unique = np.unique(labels)
		for label in labels_unique:
			this_label_instances = instances[labels == label]
			self.centroids[label] = find_centroid(this_label_instances)

	def predict_one(self, instance):
		"""Predict label for a single instance."""
		distances = np.ones(len(self.centroids))
		for i, centroid in enumerate(self.centroids.values()):
			distances[i] = euclidean_distance(instance, centroid)
		return list(self.centroids.keys())[distances.argmin()]

	def predict(self, instances):
		"""Predict label for multiple instances."""
		return np.asarray(list(map(self.predict_one, instances)))
