#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from matplotlib.mlab import PCA

import matplotlib.pyplot as plt
import numpy as np
import argparse
import re

# Colors and markers that can be used with matplotlib.
COLORS = list(reversed(['blue', 'red', 'yellow', 'black', 'white', 'green', 'cyan', 'magenta']))
MARKERS = list(reversed(['o', 's', '^', 'p', 'h', '8', 'D', 'x']))

def main():
	with open(args.iris_database) as iris_file:
		lines = iris_file.readlines()
		lines = list(filter(lambda l: l != '\n', lines)) # Ignore empty lines
		iris_data = list(map(lambda l: l.strip().split(','), lines))

	# Create matrix with data only (stripping off the labels) for the PCA.
	data_only = list(map(lambda instance: instance[:-1], iris_data))
	data_np = np.asarray(data_only, dtype=float)

	# Labels list
	labels_only = list(map(lambda instance: instance[-1], iris_data))
	labels_unique = list(set(labels_only))

	# The results from applying the PCA.
	results = PCA(data_np)

	# Points (coordinates) related to their label. E.g. ([1,2,3,4], 'Iris-setosa').
	points_and_labels = list(zip(results.Y, labels_only))

	# Matplotlib axes (Each 'ax' is a set of points for a certain class).
	axes = []

	# For each label, plot the points on graph.
	for label in labels_unique:
		# Filter only those which belong to this label.
		points = filter(lambda pl: pl[1] == label, points_and_labels)

		# Grap the points only.
		points = list(map(lambda l: l[0], points))

		# Separate the coordinates for each point.
		graph_xs = list(map(lambda pcs: pcs[0], points))
		graph_ys = list(map(lambda pcs: pcs[1], points))

		# Plot the points.
		ax = plt.scatter(graph_xs, graph_ys, s=30, c=COLORS.pop(), marker=MARKERS.pop())
		axes.append(ax)

	plt.legend(axes, labels_unique)
	plt.title('Iris Database')
	plt.xlabel('PC 1')
	plt.ylabel('PC 2')

	plt.show()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('iris_database', help='The iris database (http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data).')
	args = parser.parse_args(['/home/possatti/Documents/Mestrado/am/lista1/iris/iris.data'])
	# args = parser.parse_args()
	main()
