#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Esse script busca a média e variância de cada um dos atributos do banco de dados "IRIS".

Result:
Sepal length mean: 5.843333333333335
Sepal length variance: 0.6811222222222222

Sepal width mean: 3.0540000000000007
Sepal width variance: 0.1867506666666667

Petal length mean: 3.7586666666666693
Petal length variance: 3.0924248888888854

Petal width mean: 1.1986666666666672
Petal width variance: 0.5785315555555559
"""

from __future__ import print_function, division

import numpy as np
import argparse
import os


def calculate_mean_and_variance(data):
	mean = sum(data) / len(data)

	variance = 0
	for x in data:
		variance += (x - mean) ** 2
	variance /= len(data)

	return mean, variance


def main():
	sepal_length, sepal_width, petal_length, petal_width = np.loadtxt(args.iris_dataset, delimiter=',', usecols=(0,1,2,3), unpack=True, comments='\n')
	sl_mean, sl_variance = calculate_mean_and_variance(sepal_length)
	sw_mean, sw_variance = calculate_mean_and_variance(sepal_width)
	pl_mean, pl_variance = calculate_mean_and_variance(petal_length)
	pw_mean, pw_variance = calculate_mean_and_variance(petal_width)

	# Print mean and variances.
	print("""
Sepal length mean: {}
Sepal length variance: {}

Sepal width mean: {}
Sepal width variance: {}

Petal length mean: {}
Petal length variance: {}

Petal width mean: {}
Petal width variance: {}
	""".format(sl_mean, sl_variance, sw_mean, sw_variance, pl_mean, pl_variance, pw_mean, pw_variance))

if __name__ == '__main__':
	src_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
	default_iris_location = os.path.realpath(os.path.join(src_dir, '..', 'bases', 'iris.txt'))

	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--iris-dataset', default=default_iris_location, help='Location of the iris dataset (http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data).')
	args = parser.parse_args()
	main()
