#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import os

print(sys.version)


def main():
	# Read dataset.
	dataset = np.loadtxt(args.iris_dataset, delimiter=',', dtype=bytes)
	data = dataset[:,:-1].astype(float)
	labels = dataset[:,-1].astype(str)
	labels_unique = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']

	# Prepare histogram data.
	# colors = ['blue', 'green', 'red']
	colors = ['yellow', 'green', 'red']
	sl = np.vstack((data[labels=='Iris-setosa',0], data[labels=='Iris-versicolor',0], data[labels=='Iris-virginica',0], )).transpose()
	sw = np.vstack((data[labels=='Iris-setosa',1], data[labels=='Iris-versicolor',1], data[labels=='Iris-virginica',1], )).transpose()
	pl = np.vstack((data[labels=='Iris-setosa',2], data[labels=='Iris-versicolor',2], data[labels=='Iris-virginica',2], )).transpose()
	pw = np.vstack((data[labels=='Iris-setosa',3], data[labels=='Iris-versicolor',3], data[labels=='Iris-virginica',3], )).transpose()

	# Draw figure.
	fig, axes = plt.subplots(2, 2)
	# fig, axes = plt.subplots(1, 4)
	axes_flat = axes.flatten()
	axes_flat[0].hist(sl, bins=16, histtype='bar', normed=1, color=colors, label=labels_unique)
	axes_flat[1].hist(sw, bins=16, histtype='bar', normed=1, color=colors, label=labels_unique)
	axes_flat[2].hist(pl, bins=16, histtype='bar', normed=1, color=colors, label=labels_unique)
	axes_flat[3].hist(pw, bins=16, histtype='bar', normed=1, color=colors, label=labels_unique)
	axes_flat[0].set_title('Sepal Length')
	axes_flat[1].set_title('Sepal Width')
	axes_flat[2].set_title('Petal Length')
	axes_flat[3].set_title('Petal Width')
	legend_handles, legend_labels = axes_flat[3].get_legend_handles_labels()
	fig.legend(legend_handles, legend_labels, 'lower center', ncol=3)
	# fig.suptitle('Iris Dataset')
	if args.save:
		plt.savefig(args.save)
	else:
		plt.show()


if __name__ == '__main__':
	src_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
	default_iris_location = os.path.realpath(os.path.join(src_dir, '..', 'bases', 'iris.txt'))

	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--iris-dataset', default=default_iris_location, help='The iris dataset (http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data).')
	parser.add_argument('-s', '--save', metavar='file_path', help='Where to save the produced figure.')
	args = parser.parse_args()
	main()
