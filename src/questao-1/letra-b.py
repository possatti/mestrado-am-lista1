#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Esse script busca a média e variância de cada um dos atributos do banco de dados
"IRIS", para cada uma das classes.

Output:

Iris-setosa summary
attribute,     mean,  variance
Sepal length,  5.006, 0.122
Sepal width,   3.418, 0.142
Petal length,  1.464, 0.030
Petal width,   0.244, 0.011

Iris-versicolor summary
attribute,     mean,  variance
Sepal length,  5.936, 0.261
Sepal width,   2.770, 0.097
Petal length,  4.260, 0.216
Petal width,   1.326, 0.038

Iris-virginica summary
attribute,     mean,  variance
Sepal length,  6.588, 0.396
Sepal width,   2.974, 0.102
Petal length,  5.552, 0.298
Petal width,   2.026, 0.074
"""

from __future__ import print_function, division

import numpy as np
import argparse
import os


def calculate_mean_and_variance(data):
	# Convert strings to floats, if necessary.
	data = list(map(lambda x: float(x), data))

	mean = sum(data) / len(data)

	variance = 0
	for x in data:
		variance += (x - mean) ** 2
	variance /= len(data)

	return mean, variance


def main():
	with open(args.iris_dataset) as iris_file:
		lines = iris_file.readlines()
		lines = list(filter(lambda l: l != '\n', lines)) # Ignore empty lines
		iris_data = list(map(lambda l: l.strip().split(','), lines))
		setosas = [instance for instance in iris_data if instance[-1] == 'Iris-setosa']
		versicolors = [instance for instance in iris_data if instance[-1] == 'Iris-versicolor']
		virginicas = [instance for instance in iris_data if instance[-1] == 'Iris-virginica']

	for flowers in setosas, versicolors, virginicas:
		sepal_length, sepal_width, petal_length, petal_width, labels = zip(*flowers)
		sl_mean, sl_variance = calculate_mean_and_variance(sepal_length)
		sw_mean, sw_variance = calculate_mean_and_variance(sepal_width)
		pl_mean, pl_variance = calculate_mean_and_variance(petal_length)
		pw_mean, pw_variance = calculate_mean_and_variance(petal_width)

		print('{} summary'.format(labels[0]))
		print('attribute,     mean,  variance')
		print('Sepal length,  {:.3f}, {:.3f}'.format(sl_mean, sl_variance))
		print('Sepal width,   {:.3f}, {:.3f}'.format(sw_mean, sw_variance))
		print('Petal length,  {:.3f}, {:.3f}'.format(pl_mean, pl_variance))
		print('Petal width,   {:.3f}, {:.3f}'.format(pw_mean, pw_variance))
		print()

if __name__ == '__main__':
	src_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
	default_iris_location = os.path.realpath(os.path.join(src_dir, '..', 'bases', 'iris.txt'))

	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--iris-dataset', default=default_iris_location, help='The iris dataset (http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data).')
	args = parser.parse_args()
	main()
