#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.dimensionality import pca

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import re
import os


# Colors and markers that can be used with matplotlib.
COLORS = list(reversed(['blue', 'green', 'red', 'yellow', 'black', 'white', 'cyan', 'magenta']))
MARKERS = list(reversed(['o', 's', '^', 'p', 'h', '8', 'D', 'x']))

def main():
	print('Reading dataset \'{}\'...'.format(args.iris_dataset), file=sys.stderr)
	with open(args.iris_dataset) as iris_file:
		lines = iris_file.readlines()
		lines = list(filter(lambda l: l != '\n', lines)) # Ignore empty lines
		iris_data = list(map(lambda l: l.strip().split(','), lines))
	all_iris_data = np.array(iris_data)
	data = all_iris_data[:,:-1].astype(float)
	labels = all_iris_data[:,-1]

	# List of labels without repetition.
	labels_unique = list(set(labels))

	# The results from applying the PCA.
	print('Calculating PCA...', file=sys.stderr)
	pcs = pca(data)

	# Points (coordinates) related to their label. E.g. ([1,2,3,4], 'Iris-setosa').
	points_and_labels = list(zip(pcs, labels))

	# For each label, plot the points on graph.
	print('Plotting figure...', file=sys.stderr)
	axes = []
	for label in labels_unique:
		# Filter only those which belong to this label.
		points = filter(lambda pl: pl[1] == label, points_and_labels)
		points = list(map(lambda pl: pl[0], points))
		points = np.array(points)

		# Plot the points.
		graph_xs = points[:,0]
		graph_ys = points[:,1]
		ax = plt.scatter(graph_xs, graph_ys, s=30, c=COLORS.pop(), marker=MARKERS.pop())
		axes.append(ax)

	plt.legend(axes, labels_unique)
	plt.title('Iris Database')
	plt.xlabel('PC 1')
	plt.ylabel('PC 2')

	if args.save:
		plt.savefig(args.save)
	else:
		plt.show()

if __name__ == '__main__':
	# Default paths
	src_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
	default_iris_location = os.path.realpath(os.path.join(src_dir, '..', 'bases', 'iris.txt'))

	# Options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--iris-dataset', default=default_iris_location, help='The iris dataset (http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data).')
	parser.add_argument('-s', '--save', metavar='file_path', help='Save the figure to this path.')
	args = parser.parse_args()

	main()
