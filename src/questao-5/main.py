#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.regression import linear_regression
from am.utils import root_mean_square_error

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)


def main():
	dataset = np.genfromtxt(args.runner_dataset)
	years, values = dataset.transpose()

	if args.debug:
		print('years:\n', years)
		print('values:\n', values)

	if args.use_numpy:
		A = np.vstack([np.ones(len(years)), years]).T
		w = np.linalg.lstsq(A, values)[0]
	else:
		A = np.vstack([np.ones(len(years)), years]).T
		w = linear_regression(A, values)
	b, a = w

	# Plot the points, and the linear model.
	plt.scatter(years, values, c='black', marker='+')
	plt.plot(years, years*a + b, 'b-', label='Linear Model')
	plt.plot(2016, 2016*a + b, 'ro')

	print('Estimated linear model: y = {:.4f} * x + {:.4f}'.format(a, b))
	print('RMSE for the linar model: {}'.format(root_mean_square_error(years, values, A.dot(w))))
	print('Prediction for 2016: {:.2f}'.format(2016*a + b))

	if args.save:
		plt.savefig(args.save)
	else:
		plt.show()

if __name__ == '__main__':
	# Default paths.
	root_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..'))
	default_runner_location = os.path.join(root_dir, 'bases', 'Runner_num.txt')
	default_fig_path = __file__.replace('.py', '.png')

	# Arguments and options.
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--runner-dataset', default=default_runner_location, help='Path to the Runner dataset.')
	parser.add_argument('-d', '--debug', action='store_true', help='Print debug information.')
	parser.add_argument('-n', '--use-numpy', action='store_true', help='Use numpy whenever possible.')
	parser.add_argument('-s', '--save', metavar='file_path', nargs='?', const=default_fig_path, help='Save the produced figure to the specified path.')
	args = parser.parse_args()

	if args.debug:
		print(args, file=sys.stderr)
	main()
