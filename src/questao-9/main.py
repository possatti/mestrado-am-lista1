#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.classification import KNN
from am.utils import confusion_matrix

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import re
import os

print(sys.version, file=sys.stderr)

buying   = ['vhigh', 'high', 'med', 'low']
maint    = ['vhigh', 'high', 'med', 'low']
doors    = ['2', '3', '4', '5more']
persons  = ['2', '4', 'more']
lug_boot = ['small', 'med', 'big']
safety   = ['low', 'med', 'high']
labels   = ['unacc', 'acc', 'good', 'vgood']

converters = {
	0: lambda s: buying.index(s.decode()),
	1: lambda s: maint.index(s.decode()),
	2: lambda s: doors.index(s.decode()),
	3: lambda s: persons.index(s.decode()),
	4: lambda s: lug_boot.index(s.decode()),
	5: lambda s: safety.index(s.decode()),
	6: lambda s: labels.index(s.decode()),
}

def main():
	np.random.seed(0)
	dataset = np.loadtxt(args.car_dataset, delimiter=',', converters=converters, dtype=int)
	np.random.shuffle(dataset)

	# Split dataset and prepare iterations.
	folds = np.split(dataset, 3)
	iterations = [
		[0,1,2],
		[2,0,1],
		[1,2,0],
	]

	ks = np.arange(1,20)
	conf_matrices = np.empty(shape=(3,4,4))
	precisions = np.empty(shape=(3,4))
	recalls = np.empty(shape=(3,4))
	accs = []
	for i, iteration in enumerate(iterations):
		print('Current iteration:', iteration, file=sys.stderr)
		training = folds[iteration[0]]
		validation = folds[iteration[1]]
		testing = folds[iteration[2]]

		knn = KNN()
		knn.train(training[:,:-1], training[:,-1], validation[:,:-1], validation[:,-1], ks=ks)
		predictions = knn.predict(testing[:,:-1])

		conf_matrix = confusion_matrix(predictions, testing[:,-1], list(range(len(labels))))
		accuracy = conf_matrix.trace() / conf_matrix.sum()
		precision = conf_matrix.diagonal() / conf_matrix.sum(axis=0)
		recall = conf_matrix.diagonal() / conf_matrix.sum(axis=1)

		conf_matrices[i] = conf_matrix
		precisions[i] = precision
		recalls[i] = recall
		accs.append(accuracy)

		# print('Predictions:\n', predictions, file=sys.stderr)
		print('Chosen k:', knn.k, file=sys.stderr)
		print('Confusion matrix:\n', conf_matrix, file=sys.stderr)
		print('Multi-class precision:', precision, file=sys.stderr)
		print('Multi-class recall:', recall, file=sys.stderr)
		print('Accuracy:', accuracy, file=sys.stderr)
		print(file=sys.stderr)

	average_conf_matrix = conf_matrices.mean(axis=0)
	average_conf_matrix /= average_conf_matrix.sum(axis=1)[:,np.newaxis]

	print('Average confusion matrix:\n', average_conf_matrix)
	print('Average macro-precision:', precisions.mean())
	print('Average macro-recall:', recalls.mean())
	# print('Average micro-precision:', (conf_matrices.sum(axis=0).diagonal() / conf_matrices.sum(axis=0).sum(axis=0)).mean())
	# print('Average micro-recall:', (conf_matrices.sum(axis=0).diagonal() / conf_matrices.sum(axis=0).sum(axis=1)).mean())
	print('Average accuracy:', np.mean(accs))


if __name__ == '__main__':
	# Arguments and options
	default_car_location = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'bases', 'car.txt'))
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--car-dataset', metavar='file_path', default=default_car_location, help='Path to the Car Evaluation dataset.')
	args = parser.parse_args()
	main()
