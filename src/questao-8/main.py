#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.classification import NN

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import re
import os

print(sys.version, file=sys.stderr)


def main():
	# Initialize data.
	diamond  = np.array([60, 150])
	square   = np.array([50, 50])
	triangle = np.array([160, 40])
	circle   = np.array([190, 130])
	data     = np.vstack((diamond, square, triangle))
	all_data = np.vstack((diamond, square, triangle, circle))
	labels   = np.array(['Diamond', 'Square', 'Triangle'])

	# Test the circle using the euclidean distance.
	nn = NN()
	nn.train(data, labels)
	euclidean_prediction = nn.predict_one(circle, NN.EUCLIDEAN_DISTANCE)
	cosine_prediction = nn.predict_one(circle, NN.COSINE_SIMILARITY)
	print('Prediction using euclidean distance:', euclidean_prediction)
	print('Prediction using cosine similarity:', cosine_prediction)

	if args.create_figures:
		# Plot a beautiful figure for euclidean distance... |^-^|
		plt.plot(diamond[0],  diamond[1],  'D', c='gray')
		plt.plot(square[0],   square[1],   's', c='blue')
		plt.plot(triangle[0], triangle[1], '^', c='red')
		plt.plot(circle[0],   circle[1],   'o', c='green')
		center = data.mean(axis=0)
		# plt.plot(center[0], center[1], 'o')
		plt.plot([center[0],0], [center[1],130], c='black')
		plt.plot([center[0],110], [center[1],0], c='black')
		plt.plot([center[0],200], [center[1],160], c='black')
		plt.xlim(0, all_data[:,0].max()+10)
		plt.ylim(0, all_data[:,1].max()+10)
		if args.save_euclidean_figure:
			plt.savefig(args.save_euclidean_figure)
		else:
			plt.show()

		# Plot a beautiful figure for cosine similarity... |^-^|
		plt.clf() # Clear previous figure.
		plt.plot(diamond[0],  diamond[1],  'D', c='gray')
		plt.plot(square[0],   square[1],   's', c='blue')
		plt.plot(triangle[0], triangle[1], '^', c='red')
		plt.plot(circle[0],   circle[1],   'o', c='green')
		plt.plot([0,90], [0,160], c='black')
		plt.plot([0,210], [0,90], c='black')
		plt.xlim(0, all_data[:,0].max()+10)
		plt.ylim(0, all_data[:,1].max()+10)
		if args.save_cosine_figure:
			plt.savefig(args.save_cosine_figure)
		else:
			plt.show()

if __name__ == '__main__':
	# Arguments and options
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('-f', '--create-figures', action='store_true', help='Create the voronoi diagrams.')
	parser.add_argument('-e', '--save-euclidean-figure', metavar='file_path', help='Where to save the figure to.')
	parser.add_argument('-c', '--save-cosine-figure', metavar='file_path', help='Where to save the figure to.')
	args = parser.parse_args()
	main()
