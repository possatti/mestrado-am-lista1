#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from am.classification import NN

import matplotlib.pyplot as plt
import numpy as np
import argparse
import json
import sys
import re
import os

print(sys.version, file=sys.stderr)

def sequential_forward_selection(training_data, training_labels, validation_data, validation_labels, n_features):
	selected_features = []
	remaining_features = list(range(13))
	for i in range(n_features):
		accs = np.empty(len(remaining_features))
		for f, feature in enumerate(remaining_features):
			nn = NN()
			nn.train(training_data[:, selected_features + [feature]], training_labels)
			predictions = nn.predict(validation_data[:, selected_features + [feature]])
			accs[f] = (predictions == validation_labels).sum() / len(validation_labels)
		# print('remaining_features:', remaining_features, file=sys.stderr)
		# print('accs:', accs, file=sys.stderr)
		best_f = np.argmax(accs)
		# print('best_f:', best_f, file=sys.stderr)
		best_feature = remaining_features.pop(best_f)
		# print('best_feature:', best_feature, file=sys.stderr)
		selected_features.append(best_feature)
	return np.array(selected_features)

def sequential_backward_elimination(training_data, training_labels, validation_data, validation_labels, n_features):
	remaining_features = list(range(13))
	for i in range(13 - n_features):
		accs = np.empty(len(remaining_features))
		for f, feature in enumerate(remaining_features):
			current_features = list(set(remaining_features) - set([feature]))
			nn = NN()
			nn.train(training_data[:, current_features], training_labels)
			predictions = nn.predict(validation_data[:, current_features])
			accs[f] = (predictions == validation_labels).sum() / len(validation_labels)
		# print('remaining_features:', remaining_features, file=sys.stderr)
		# print('accs:', accs, file=sys.stderr)
		worst_f = np.argmin(accs)
		worst_feature = remaining_features.pop(worst_f)
		# print('worst_f:', worst_f, file=sys.stderr)
		# print('worst_feature:', worst_feature, file=sys.stderr)
	return np.array(remaining_features)

def test(training_data, training_labels, testing_data, testing_labels, features):
	nn = NN()
	nn.train(training_data[:, features], training_labels)
	predictions = nn.predict(testing_data[:, features])
	acc = (predictions == testing_labels).sum() / len(testing_labels)
	return acc

def main():
	dataset = np.loadtxt(args.wine_dataset, delimiter=',')
	np.random.seed(0)
	np.random.shuffle(dataset)

	# Normalize all attributes to the range of 0 to 1.
	minimums = dataset[:,1:].min(axis=0)
	maximums = dataset[:,1:].max(axis=0)
	dataset[:,1:] = (dataset[:,1:] - minimums) / (maximums - minimums)

	# np.set_printoptions(precision=3, suppress=True)
	# print(dataset[:10])
	# exit()

	# Prepare groups for the process of stratification. Each group contains a class.
	group1 = dataset[dataset[:,0] == 1]
	group2 = dataset[dataset[:,0] == 2]
	group3 = dataset[dataset[:,0] == 3]
	# print('len(group1):', len(group1), file=sys.stderr)
	# print('len(group2):', len(group2), file=sys.stderr)
	# print('len(group3):', len(group3), file=sys.stderr)

	training1, validation1, testing1 = np.split(group1, [int(len(group1)*0.6), int(len(group1)*0.8)])
	training2, validation2, testing2 = np.split(group2, [int(len(group2)*0.6), int(len(group2)*0.8)])
	training3, validation3, testing3 = np.split(group3, [int(len(group3)*0.6), int(len(group3)*0.8)])
	# print('Sizes1: ', len(training1), len(validation1), len(testing1), file=sys.stderr)
	# print('Sizes2: ', len(training2), len(validation2), len(testing2), file=sys.stderr)
	# print('Sizes3: ', len(training3), len(validation3), len(testing3), file=sys.stderr)

	# Prepare training, validation, and testing sets.
	training = np.vstack((training1, training2, training3))
	validation = np.vstack((validation1, validation2, validation3))
	testing = np.vstack((testing1, testing2, testing3))
	np.random.shuffle(training)
	np.random.shuffle(validation)
	np.random.shuffle(testing)
	print('len(training), len(validation), len(testing):', len(training), len(validation), len(testing), file=sys.stderr)
	training_labels, training_data = training[:,0], training[:,1:]
	validation_labels, validation_data = validation[:,0], validation[:,1:]
	testing_labels, testing_data = testing[:,0], testing[:,1:]

	# Merge training and validation together, to use to train for the tests.
	train_val_data = np.vstack((training_data, validation_data))
	train_val_labels = np.hstack((training_labels, validation_labels))


	if args.letter_a:
		print('Selecting 5 features using SFS...', file=sys.stderr)
		sfs_selected_features = sequential_forward_selection(training_data, training_labels, validation_data, validation_labels, 5)
		sfs_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sfs_selected_features)
		print('Selecting 5 features using SBE...', file=sys.stderr)
		sbe_selected_features = sequential_backward_elimination(training_data, training_labels, validation_data, validation_labels, 5)
		sbe_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sbe_selected_features)
		print('Selected features + 1 (SFS):', sfs_selected_features + 1)
		print('Test accuracy (SFS):', sfs_acc_test)
		print('Selected features + 1 (SBE):', sbe_selected_features + 1)
		print('Test accuracy (SBE):', sbe_acc_test)

	if args.letter_b:
		print('Selecting 10 features using SFS...', file=sys.stderr)
		sfs_selected_features = sequential_forward_selection(training_data, training_labels, validation_data, validation_labels, 10)
		sfs_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sfs_selected_features)
		print('Selecting 10 features using SBE...', file=sys.stderr)
		sbe_selected_features = sequential_backward_elimination(training_data, training_labels, validation_data, validation_labels, 10)
		sbe_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sbe_selected_features)
		print('Selected features + 1 (SFS):', sfs_selected_features + 1)
		print('Test accuracy (SFS):', sfs_acc_test)
		print('Selected features + 1 (SBE):', sbe_selected_features + 1)
		print('Test accuracy (SBE):', sbe_acc_test)

	if args.letter_c:
		print('Selecting 5 features using SFS...', file=sys.stderr)
		sfs_selected_features = sequential_forward_selection(train_val_data, train_val_labels, train_val_data, train_val_labels, 5)
		sfs_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sfs_selected_features)
		print('Selecting 5 features using SBE...', file=sys.stderr)
		sbe_selected_features = sequential_backward_elimination(train_val_data, train_val_labels, train_val_data, train_val_labels, 5)
		sbe_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sbe_selected_features)
		print('Selected features + 1 (SFS):', sfs_selected_features + 1)
		print('Test accuracy (SFS):', sfs_acc_test)
		print('Selected features + 1 (SBE):', sbe_selected_features + 1)
		print('Test accuracy (SBE):', sbe_acc_test)

		print('Selecting 10 features using SFS...', file=sys.stderr)
		sfs_selected_features = sequential_forward_selection(train_val_data, train_val_labels, train_val_data, train_val_labels, 10)
		sfs_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sfs_selected_features)
		print('Selecting 10 features using SBE...', file=sys.stderr)
		sbe_selected_features = sequential_backward_elimination(train_val_data, train_val_labels, train_val_data, train_val_labels, 10)
		sbe_acc_test = test(train_val_data, train_val_labels, testing_data, testing_labels, sbe_selected_features)
		print('Selected features + 1 (SFS):', sfs_selected_features + 1)
		print('Test accuracy (SFS):', sfs_acc_test)
		print('Selected features + 1 (SBE):', sbe_selected_features + 1)
		print('Test accuracy (SBE):', sbe_acc_test)

	# Use random features.
	if args.random_features:
		for i in range(20):
			random_features = np.arange(0,13)
			np.random.shuffle(random_features)
			random_features = random_features[:np.random.randint(1,13)]
			print('Random features:', random_features)
			random_acc = test(train_val_data, train_val_labels, testing_data, testing_labels, random_features)
			print('Accuracy:', random_acc)
			print()


if __name__ == '__main__':
	# Arguments and options
	default_wine_location = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'bases', 'wine.txt'))

	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--wine-dataset', default=default_wine_location, help='Path to the wine dataset.')

	letter_group = parser.add_mutually_exclusive_group(required=True)
	letter_group.add_argument('-a', '--letter-a', action='store_true')
	letter_group.add_argument('-b', '--letter-b', action='store_true')
	letter_group.add_argument('-c', '--letter-c', action='store_true')
	letter_group.add_argument('-r', '--random-features', action='store_true')

	args = parser.parse_args()

	main()
